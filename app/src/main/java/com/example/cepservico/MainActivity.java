package com.example.cepservico;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText edCep;
    TextView tvCep;

    BuscaCep servico;

    ServiceConnection conexao = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            servico = ((BuscaCep.GeradorBinder) iBinder).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            servico = null;
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        Intent it = new Intent(getApplicationContext(), BuscaCep.class);
        bindService(it, conexao, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        unbindService(conexao);
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edCep = (EditText) findViewById(R.id.edCep);
        tvCep = (TextView) findViewById(R.id.tvCep);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void consultar(View view) {
        if(servico != null) {
            tvCep.setText(servico.buscar(edCep.getText().toString()).toString());
        }
    }
}