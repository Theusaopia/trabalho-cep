package com.example.cepservico;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class BuscaCep extends Service {
    GeradorBinder binder;

    public BuscaCep() {
        binder = new GeradorBinder();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class GeradorBinder extends Binder {
        public BuscaCep getService() {
            return BuscaCep.this;
        }
    }

    public Cep buscar(String cep) {
        try {
            URL url = new URL("https://viacep.com.br/ws/" + cep + "/json/");
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.connect();
            if (conn.getResponseCode() == 200) {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String resp = "";
                String linha;
                do {
                    linha = reader.readLine();
                    if (linha != null) {
                        resp += linha;
                    }
                } while (linha != null);
                Gson gson = new GsonBuilder().create();
                Cep dc = gson.fromJson(resp, Cep.class);
                return dc;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
            return null;
        }
        return null;
    }
}